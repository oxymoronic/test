<?php

/* base.html.twig */
class __TwigTemplate_8c574b2cd07997c67feb95440e9fbcac5a26e98453a48814b91963c6fbd261d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a011f4b921fb7de64f21080a20fa015a94a91b26acd23feb783442fa016672d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a011f4b921fb7de64f21080a20fa015a94a91b26acd23feb783442fa016672d->enter($__internal_5a011f4b921fb7de64f21080a20fa015a94a91b26acd23feb783442fa016672d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_5a011f4b921fb7de64f21080a20fa015a94a91b26acd23feb783442fa016672d->leave($__internal_5a011f4b921fb7de64f21080a20fa015a94a91b26acd23feb783442fa016672d_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6ec9c974d41750adc9270aab5f00533e6ed50a1637f2009820e0426df8a3365a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ec9c974d41750adc9270aab5f00533e6ed50a1637f2009820e0426df8a3365a->enter($__internal_6ec9c974d41750adc9270aab5f00533e6ed50a1637f2009820e0426df8a3365a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6ec9c974d41750adc9270aab5f00533e6ed50a1637f2009820e0426df8a3365a->leave($__internal_6ec9c974d41750adc9270aab5f00533e6ed50a1637f2009820e0426df8a3365a_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_92a28259727e48719cc1b047ad0404977aa1d5640749df278f659ab1aae60970 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92a28259727e48719cc1b047ad0404977aa1d5640749df278f659ab1aae60970->enter($__internal_92a28259727e48719cc1b047ad0404977aa1d5640749df278f659ab1aae60970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_92a28259727e48719cc1b047ad0404977aa1d5640749df278f659ab1aae60970->leave($__internal_92a28259727e48719cc1b047ad0404977aa1d5640749df278f659ab1aae60970_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_bcc107c4dc8ec68ff86cfa1d7957e6e23bc6638ed4a35a21edb3420de2ce46ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcc107c4dc8ec68ff86cfa1d7957e6e23bc6638ed4a35a21edb3420de2ce46ff->enter($__internal_bcc107c4dc8ec68ff86cfa1d7957e6e23bc6638ed4a35a21edb3420de2ce46ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_bcc107c4dc8ec68ff86cfa1d7957e6e23bc6638ed4a35a21edb3420de2ce46ff->leave($__internal_bcc107c4dc8ec68ff86cfa1d7957e6e23bc6638ed4a35a21edb3420de2ce46ff_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5bc1854bb099589767d9a1bf6201f151790b7b508aaf2ee3c7672dc5d1539608 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5bc1854bb099589767d9a1bf6201f151790b7b508aaf2ee3c7672dc5d1539608->enter($__internal_5bc1854bb099589767d9a1bf6201f151790b7b508aaf2ee3c7672dc5d1539608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5bc1854bb099589767d9a1bf6201f151790b7b508aaf2ee3c7672dc5d1539608->leave($__internal_5bc1854bb099589767d9a1bf6201f151790b7b508aaf2ee3c7672dc5d1539608_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/test/app/Resources/views/base.html.twig");
    }
}
