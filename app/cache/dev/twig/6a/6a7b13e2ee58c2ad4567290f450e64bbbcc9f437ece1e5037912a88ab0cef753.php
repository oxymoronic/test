<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_88389321ea49ce64d40295ac6eab6a112ce85f19fa9f65eca369a5a46e5fe473 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90a4fc856d9f1b4c8bd8c1a0ab2cad26ba07728b2540138c834f2fd1ac5c5720 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90a4fc856d9f1b4c8bd8c1a0ab2cad26ba07728b2540138c834f2fd1ac5c5720->enter($__internal_90a4fc856d9f1b4c8bd8c1a0ab2cad26ba07728b2540138c834f2fd1ac5c5720_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_90a4fc856d9f1b4c8bd8c1a0ab2cad26ba07728b2540138c834f2fd1ac5c5720->leave($__internal_90a4fc856d9f1b4c8bd8c1a0ab2cad26ba07728b2540138c834f2fd1ac5c5720_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
