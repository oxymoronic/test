<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_d131e07d58d2b074a1531792361aabdf3a7f3e34149801ba14d532285cef52e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f3a70a7af5e59ee279c91c72b361956f7a2da1bbed36822a73cb39d80d09f10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f3a70a7af5e59ee279c91c72b361956f7a2da1bbed36822a73cb39d80d09f10->enter($__internal_4f3a70a7af5e59ee279c91c72b361956f7a2da1bbed36822a73cb39d80d09f10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_4f3a70a7af5e59ee279c91c72b361956f7a2da1bbed36822a73cb39d80d09f10->leave($__internal_4f3a70a7af5e59ee279c91c72b361956f7a2da1bbed36822a73cb39d80d09f10_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
