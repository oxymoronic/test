<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_f28b445e5d9b82a1b786f3293d93985aeb3b7ca3d5d1f80f632d2c7b2bb32952 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8684bc5ed1fb016a4c5f88ebefb93e7a2eb2c75d781c0277fa9a7ba4eab88d9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8684bc5ed1fb016a4c5f88ebefb93e7a2eb2c75d781c0277fa9a7ba4eab88d9f->enter($__internal_8684bc5ed1fb016a4c5f88ebefb93e7a2eb2c75d781c0277fa9a7ba4eab88d9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_8684bc5ed1fb016a4c5f88ebefb93e7a2eb2c75d781c0277fa9a7ba4eab88d9f->leave($__internal_8684bc5ed1fb016a4c5f88ebefb93e7a2eb2c75d781c0277fa9a7ba4eab88d9f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
