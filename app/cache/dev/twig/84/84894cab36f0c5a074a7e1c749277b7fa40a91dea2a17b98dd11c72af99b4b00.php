<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_7415b8e9b954a9751873a6bf90e61f9f13fe85a48aedba34d8ebd2c4b935e8e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aec1cea6a6fd4494410a15bdb9dd7dfa7e8894d4c25aa8e9d113427a5adb9718 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aec1cea6a6fd4494410a15bdb9dd7dfa7e8894d4c25aa8e9d113427a5adb9718->enter($__internal_aec1cea6a6fd4494410a15bdb9dd7dfa7e8894d4c25aa8e9d113427a5adb9718_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_aec1cea6a6fd4494410a15bdb9dd7dfa7e8894d4c25aa8e9d113427a5adb9718->leave($__internal_aec1cea6a6fd4494410a15bdb9dd7dfa7e8894d4c25aa8e9d113427a5adb9718_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
