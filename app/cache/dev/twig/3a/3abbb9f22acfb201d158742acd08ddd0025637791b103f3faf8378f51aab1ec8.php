<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_b23a40c21bd5ac5110df0ebffeca8306c4dd5b6a212beed3712d8f3ba32e6279 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d0c8758ce3865379cf819333955978f76de4dd418a959cca9b097913bc2af82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d0c8758ce3865379cf819333955978f76de4dd418a959cca9b097913bc2af82->enter($__internal_6d0c8758ce3865379cf819333955978f76de4dd418a959cca9b097913bc2af82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d0c8758ce3865379cf819333955978f76de4dd418a959cca9b097913bc2af82->leave($__internal_6d0c8758ce3865379cf819333955978f76de4dd418a959cca9b097913bc2af82_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_897a7358ff03604b5836f8cb1cff27e34e5456747e5637b4668362fbd3aba6b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_897a7358ff03604b5836f8cb1cff27e34e5456747e5637b4668362fbd3aba6b2->enter($__internal_897a7358ff03604b5836f8cb1cff27e34e5456747e5637b4668362fbd3aba6b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_897a7358ff03604b5836f8cb1cff27e34e5456747e5637b4668362fbd3aba6b2->leave($__internal_897a7358ff03604b5836f8cb1cff27e34e5456747e5637b4668362fbd3aba6b2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_e7a6c33d8cffcea9cf4c8fb80bff50cf7f77ddcd5296b0475954dc7740e06642 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7a6c33d8cffcea9cf4c8fb80bff50cf7f77ddcd5296b0475954dc7740e06642->enter($__internal_e7a6c33d8cffcea9cf4c8fb80bff50cf7f77ddcd5296b0475954dc7740e06642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_e7a6c33d8cffcea9cf4c8fb80bff50cf7f77ddcd5296b0475954dc7740e06642->leave($__internal_e7a6c33d8cffcea9cf4c8fb80bff50cf7f77ddcd5296b0475954dc7740e06642_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
