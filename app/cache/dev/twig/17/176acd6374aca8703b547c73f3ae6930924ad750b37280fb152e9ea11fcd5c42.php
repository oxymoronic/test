<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_24045d91afad867eb68ae3005894a662e77e6856d1922ab516f030c95d2c1229 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d89553b7f7a38804808aebd9c1eeb1b65bfb536e0b7272f9f305b769b56ff8d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d89553b7f7a38804808aebd9c1eeb1b65bfb536e0b7272f9f305b769b56ff8d1->enter($__internal_d89553b7f7a38804808aebd9c1eeb1b65bfb536e0b7272f9f305b769b56ff8d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d89553b7f7a38804808aebd9c1eeb1b65bfb536e0b7272f9f305b769b56ff8d1->leave($__internal_d89553b7f7a38804808aebd9c1eeb1b65bfb536e0b7272f9f305b769b56ff8d1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
