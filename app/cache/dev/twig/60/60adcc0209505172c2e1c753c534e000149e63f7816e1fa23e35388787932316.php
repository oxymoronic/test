<?php

/* :doctor:index.html.twig */
class __TwigTemplate_a6d2a42d8f0801ff6eafbf73f721dd8e24bf00c4309074bdfc67faa111678be6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":doctor:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bac04bcc6388ab514d931c5076ee0f197c6a54206b642e98e46018c5f4785c1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bac04bcc6388ab514d931c5076ee0f197c6a54206b642e98e46018c5f4785c1f->enter($__internal_bac04bcc6388ab514d931c5076ee0f197c6a54206b642e98e46018c5f4785c1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":doctor:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bac04bcc6388ab514d931c5076ee0f197c6a54206b642e98e46018c5f4785c1f->leave($__internal_bac04bcc6388ab514d931c5076ee0f197c6a54206b642e98e46018c5f4785c1f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4e952a3b753b7b436c01076f481521b85086bd75897ce0b56f12aafc07ab0a03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e952a3b753b7b436c01076f481521b85086bd75897ce0b56f12aafc07ab0a03->enter($__internal_4e952a3b753b7b436c01076f481521b85086bd75897ce0b56f12aafc07ab0a03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Doctors list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "name", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_edit", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_new");
        echo "\">Create a new doctor</a>
        </li>
    </ul>
";
        
        $__internal_4e952a3b753b7b436c01076f481521b85086bd75897ce0b56f12aafc07ab0a03->leave($__internal_4e952a3b753b7b436c01076f481521b85086bd75897ce0b56f12aafc07ab0a03_prof);

    }

    public function getTemplateName()
    {
        return ":doctor:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 36,  91 => 31,  79 => 25,  73 => 22,  66 => 18,  60 => 17,  57 => 16,  53 => 15,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Doctors list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for doctor in doctors %}
            <tr>
                <td><a href=\"{{ path('doctor_show', { 'id': doctor.id }) }}\">{{ doctor.id }}</a></td>
                <td>{{ doctor.name }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('doctor_show', { 'id': doctor.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('doctor_edit', { 'id': doctor.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('doctor_new') }}\">Create a new doctor</a>
        </li>
    </ul>
{% endblock %}
", ":doctor:index.html.twig", "/var/www/html/test/app/Resources/views/doctor/index.html.twig");
    }
}
