<?php

/* :hospital:index.html.twig */
class __TwigTemplate_34aedbdc00e9c080ffaf75d757b718c0abf994d2fb193e857b4b5f2c78904d03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":hospital:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b5aa506c6712b9fe597c418640fc68f557e2ebff749c6bc43d2de32cccfe6ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b5aa506c6712b9fe597c418640fc68f557e2ebff749c6bc43d2de32cccfe6ff->enter($__internal_4b5aa506c6712b9fe597c418640fc68f557e2ebff749c6bc43d2de32cccfe6ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":hospital:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4b5aa506c6712b9fe597c418640fc68f557e2ebff749c6bc43d2de32cccfe6ff->leave($__internal_4b5aa506c6712b9fe597c418640fc68f557e2ebff749c6bc43d2de32cccfe6ff_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f3a6c74eaa7351811ffeca1881d9d8698a5d2f1961ab31ba09c4ac6aed3c9b1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3a6c74eaa7351811ffeca1881d9d8698a5d2f1961ab31ba09c4ac6aed3c9b1b->enter($__internal_f3a6c74eaa7351811ffeca1881d9d8698a5d2f1961ab31ba09c4ac6aed3c9b1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Hospitals list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["hospitals"]) ? $context["hospitals"] : $this->getContext($context, "hospitals")));
        foreach ($context['_seq'] as $context["_key"] => $context["hospital"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_show", array("id" => $this->getAttribute($context["hospital"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["hospital"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["hospital"], "name", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_show", array("id" => $this->getAttribute($context["hospital"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_edit", array("id" => $this->getAttribute($context["hospital"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hospital'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_new");
        echo "\">Create a new hospital</a>
        </li>
    </ul>
";
        
        $__internal_f3a6c74eaa7351811ffeca1881d9d8698a5d2f1961ab31ba09c4ac6aed3c9b1b->leave($__internal_f3a6c74eaa7351811ffeca1881d9d8698a5d2f1961ab31ba09c4ac6aed3c9b1b_prof);

    }

    public function getTemplateName()
    {
        return ":hospital:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 36,  91 => 31,  79 => 25,  73 => 22,  66 => 18,  60 => 17,  57 => 16,  53 => 15,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Hospitals list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for hospital in hospitals %}
            <tr>
                <td><a href=\"{{ path('hospital_show', { 'id': hospital.id }) }}\">{{ hospital.id }}</a></td>
                <td>{{ hospital.name }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('hospital_show', { 'id': hospital.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('hospital_edit', { 'id': hospital.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('hospital_new') }}\">Create a new hospital</a>
        </li>
    </ul>
{% endblock %}
", ":hospital:index.html.twig", "/var/www/html/test/app/Resources/views/hospital/index.html.twig");
    }
}
