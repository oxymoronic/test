<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_5780ab06e448634d624420d579a252602b0d6072c38a588a21af46138c485fbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ca8f6f0ca74d4e874858ffdb78825596efccfdb2606d01d4664babb3bc64cc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ca8f6f0ca74d4e874858ffdb78825596efccfdb2606d01d4664babb3bc64cc3->enter($__internal_3ca8f6f0ca74d4e874858ffdb78825596efccfdb2606d01d4664babb3bc64cc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_3ca8f6f0ca74d4e874858ffdb78825596efccfdb2606d01d4664babb3bc64cc3->leave($__internal_3ca8f6f0ca74d4e874858ffdb78825596efccfdb2606d01d4664babb3bc64cc3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
