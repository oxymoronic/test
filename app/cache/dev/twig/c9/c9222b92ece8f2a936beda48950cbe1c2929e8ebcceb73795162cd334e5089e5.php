<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_310f33498a9caef15d3e37b6a6fa9443ec61b5c958081ef575e2f3ba08563ce5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bcd875a4e0cdf2d30d32ef2265c4a2a4b3d8941d3b7696fb07a6dc11f67da16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bcd875a4e0cdf2d30d32ef2265c4a2a4b3d8941d3b7696fb07a6dc11f67da16->enter($__internal_1bcd875a4e0cdf2d30d32ef2265c4a2a4b3d8941d3b7696fb07a6dc11f67da16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_1bcd875a4e0cdf2d30d32ef2265c4a2a4b3d8941d3b7696fb07a6dc11f67da16->leave($__internal_1bcd875a4e0cdf2d30d32ef2265c4a2a4b3d8941d3b7696fb07a6dc11f67da16_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
