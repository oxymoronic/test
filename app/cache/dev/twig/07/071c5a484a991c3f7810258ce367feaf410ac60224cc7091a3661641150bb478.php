<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_c8b59765689bfe38b78b4d2fcca55c0cfa7f1d8aaacc4cec704b8dd2520e8d7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c976d42c5ab3d96658816b4f7c658cfb6afc2c92f9589c9121cc0ebb0b3efdcd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c976d42c5ab3d96658816b4f7c658cfb6afc2c92f9589c9121cc0ebb0b3efdcd->enter($__internal_c976d42c5ab3d96658816b4f7c658cfb6afc2c92f9589c9121cc0ebb0b3efdcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_c976d42c5ab3d96658816b4f7c658cfb6afc2c92f9589c9121cc0ebb0b3efdcd->leave($__internal_c976d42c5ab3d96658816b4f7c658cfb6afc2c92f9589c9121cc0ebb0b3efdcd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
