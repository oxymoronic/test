<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_f89f1a446613edad90c53734c970903a28d8d368d65bc8bf99faca8650cc9675 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21a82aa797e5248e1e5e4705eea2b621bd7c6ad19d019782c0894a0606a2c3e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21a82aa797e5248e1e5e4705eea2b621bd7c6ad19d019782c0894a0606a2c3e9->enter($__internal_21a82aa797e5248e1e5e4705eea2b621bd7c6ad19d019782c0894a0606a2c3e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_21a82aa797e5248e1e5e4705eea2b621bd7c6ad19d019782c0894a0606a2c3e9->leave($__internal_21a82aa797e5248e1e5e4705eea2b621bd7c6ad19d019782c0894a0606a2c3e9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
