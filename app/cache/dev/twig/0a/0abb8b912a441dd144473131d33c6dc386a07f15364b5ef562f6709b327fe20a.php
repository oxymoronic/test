<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_f235dec1fb977f3db76fb007ae8deaee8aae2d0293815362c8c8f5193ec1b0f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a99b30ed4858769cb627b63c2c259b7b8c96cc2f7d463afd7540eaf859c7c603 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a99b30ed4858769cb627b63c2c259b7b8c96cc2f7d463afd7540eaf859c7c603->enter($__internal_a99b30ed4858769cb627b63c2c259b7b8c96cc2f7d463afd7540eaf859c7c603_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_a99b30ed4858769cb627b63c2c259b7b8c96cc2f7d463afd7540eaf859c7c603->leave($__internal_a99b30ed4858769cb627b63c2c259b7b8c96cc2f7d463afd7540eaf859c7c603_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
