<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_74c515b383288517b37af0814dc0dfe8430f16c71d16d4601d13c7283cf89d5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd8048c4d2a0d1391c9b6a82419c3bf83834347aa131e9ddf6e8db9bbc9c042f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd8048c4d2a0d1391c9b6a82419c3bf83834347aa131e9ddf6e8db9bbc9c042f->enter($__internal_dd8048c4d2a0d1391c9b6a82419c3bf83834347aa131e9ddf6e8db9bbc9c042f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_dd8048c4d2a0d1391c9b6a82419c3bf83834347aa131e9ddf6e8db9bbc9c042f->leave($__internal_dd8048c4d2a0d1391c9b6a82419c3bf83834347aa131e9ddf6e8db9bbc9c042f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
