<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_ffde524dec8f53efbb6b0c16b9ff1bb0d3919773cf372c723417d8aa630d3621 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b3d16606435256c6c5f9072ea64e25b74fdbcdf40e9f576d4d774d3dfff596b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b3d16606435256c6c5f9072ea64e25b74fdbcdf40e9f576d4d774d3dfff596b->enter($__internal_5b3d16606435256c6c5f9072ea64e25b74fdbcdf40e9f576d4d774d3dfff596b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_5b3d16606435256c6c5f9072ea64e25b74fdbcdf40e9f576d4d774d3dfff596b->leave($__internal_5b3d16606435256c6c5f9072ea64e25b74fdbcdf40e9f576d4d774d3dfff596b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
