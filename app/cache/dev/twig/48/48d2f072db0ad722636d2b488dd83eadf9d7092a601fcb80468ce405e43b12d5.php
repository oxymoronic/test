<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_09fe757a7c433182a11c4ba7c4346a30df09bd38444490616e48c3cb41e94d3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdd8d73e824d445cbaa9b56bd7d762ff1f1c6db85bc856f94cba48af61c4aaaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdd8d73e824d445cbaa9b56bd7d762ff1f1c6db85bc856f94cba48af61c4aaaf->enter($__internal_bdd8d73e824d445cbaa9b56bd7d762ff1f1c6db85bc856f94cba48af61c4aaaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_bdd8d73e824d445cbaa9b56bd7d762ff1f1c6db85bc856f94cba48af61c4aaaf->leave($__internal_bdd8d73e824d445cbaa9b56bd7d762ff1f1c6db85bc856f94cba48af61c4aaaf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
