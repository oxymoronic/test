<?php

/* :hospital:new.html.twig */
class __TwigTemplate_2e16ce7c72e6608b89e267317115e76b870fdc66367059f6cf0a850edbf2abf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":hospital:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_144c35d4aabf762277fadea338520f84a6ec29eb46c07b007d8d1b87e9003d55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_144c35d4aabf762277fadea338520f84a6ec29eb46c07b007d8d1b87e9003d55->enter($__internal_144c35d4aabf762277fadea338520f84a6ec29eb46c07b007d8d1b87e9003d55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":hospital:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_144c35d4aabf762277fadea338520f84a6ec29eb46c07b007d8d1b87e9003d55->leave($__internal_144c35d4aabf762277fadea338520f84a6ec29eb46c07b007d8d1b87e9003d55_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_16bd3fb90c738adaf01aa2e9129bc6042785de271f73ade1fd88feacb01d3925 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16bd3fb90c738adaf01aa2e9129bc6042785de271f73ade1fd88feacb01d3925->enter($__internal_16bd3fb90c738adaf01aa2e9129bc6042785de271f73ade1fd88feacb01d3925_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Hospital creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_16bd3fb90c738adaf01aa2e9129bc6042785de271f73ade1fd88feacb01d3925->leave($__internal_16bd3fb90c738adaf01aa2e9129bc6042785de271f73ade1fd88feacb01d3925_prof);

    }

    public function getTemplateName()
    {
        return ":hospital:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Hospital creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('hospital_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":hospital:new.html.twig", "/var/www/html/test/app/Resources/views/hospital/new.html.twig");
    }
}
