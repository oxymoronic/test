<?php

/* :hospital:show.html.twig */
class __TwigTemplate_9698f76bb5ee5550f49be1819511b8f8c0e98418d3ada82ecf443fcc13daf48e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":hospital:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9f39a47efa572ce6e55ad0e26de72ba44d43f36be8777ea6fb97c6a093419cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9f39a47efa572ce6e55ad0e26de72ba44d43f36be8777ea6fb97c6a093419cd->enter($__internal_d9f39a47efa572ce6e55ad0e26de72ba44d43f36be8777ea6fb97c6a093419cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":hospital:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d9f39a47efa572ce6e55ad0e26de72ba44d43f36be8777ea6fb97c6a093419cd->leave($__internal_d9f39a47efa572ce6e55ad0e26de72ba44d43f36be8777ea6fb97c6a093419cd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f69fe1332435f3e9224e09438c496dac9fcff8bbdb3e777c3c7834fa392de754 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f69fe1332435f3e9224e09438c496dac9fcff8bbdb3e777c3c7834fa392de754->enter($__internal_f69fe1332435f3e9224e09438c496dac9fcff8bbdb3e777c3c7834fa392de754_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Hospital</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["hospital"]) ? $context["hospital"] : $this->getContext($context, "hospital")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["hospital"]) ? $context["hospital"] : $this->getContext($context, "hospital")), "name", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hospital_edit", array("id" => $this->getAttribute((isset($context["hospital"]) ? $context["hospital"] : $this->getContext($context, "hospital")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_f69fe1332435f3e9224e09438c496dac9fcff8bbdb3e777c3c7834fa392de754->leave($__internal_f69fe1332435f3e9224e09438c496dac9fcff8bbdb3e777c3c7834fa392de754_prof);

    }

    public function getTemplateName()
    {
        return ":hospital:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  77 => 27,  71 => 24,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Hospital</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ hospital.id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ hospital.name }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('hospital_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('hospital_edit', { 'id': hospital.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":hospital:show.html.twig", "/var/www/html/test/app/Resources/views/hospital/show.html.twig");
    }
}
