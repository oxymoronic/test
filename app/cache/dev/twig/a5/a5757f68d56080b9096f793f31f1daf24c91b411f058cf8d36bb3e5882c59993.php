<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_9240386522abba5ee29e0eaa234b162710b76ac727da2f70a8a6d65c8348094f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18d50a424b2625f9bf7436df29dac2afe9e5fe8dc28ab8c28b50cc3231d8f322 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18d50a424b2625f9bf7436df29dac2afe9e5fe8dc28ab8c28b50cc3231d8f322->enter($__internal_18d50a424b2625f9bf7436df29dac2afe9e5fe8dc28ab8c28b50cc3231d8f322_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_18d50a424b2625f9bf7436df29dac2afe9e5fe8dc28ab8c28b50cc3231d8f322->leave($__internal_18d50a424b2625f9bf7436df29dac2afe9e5fe8dc28ab8c28b50cc3231d8f322_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.atom.twig", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
