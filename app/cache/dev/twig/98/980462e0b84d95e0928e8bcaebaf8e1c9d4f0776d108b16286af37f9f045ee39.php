<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_599e6cd1ef5a7506c66de32959449abb1331471c17a59d844e9d06e042e11e70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d01f6942335346a82893a519ffaa7a769f60ea96d5a5b7469c6147bffd68dd4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d01f6942335346a82893a519ffaa7a769f60ea96d5a5b7469c6147bffd68dd4->enter($__internal_4d01f6942335346a82893a519ffaa7a769f60ea96d5a5b7469c6147bffd68dd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d01f6942335346a82893a519ffaa7a769f60ea96d5a5b7469c6147bffd68dd4->leave($__internal_4d01f6942335346a82893a519ffaa7a769f60ea96d5a5b7469c6147bffd68dd4_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7ab7cd097bf89e9228c86b71de8bc91f467f0d97b1a685e2a4216ea49b16148c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ab7cd097bf89e9228c86b71de8bc91f467f0d97b1a685e2a4216ea49b16148c->enter($__internal_7ab7cd097bf89e9228c86b71de8bc91f467f0d97b1a685e2a4216ea49b16148c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_7ab7cd097bf89e9228c86b71de8bc91f467f0d97b1a685e2a4216ea49b16148c->leave($__internal_7ab7cd097bf89e9228c86b71de8bc91f467f0d97b1a685e2a4216ea49b16148c_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_491a6307d48c8e7b0520ad96e7e22d0e41580bba4e2ac939ce91afd0e383a348 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_491a6307d48c8e7b0520ad96e7e22d0e41580bba4e2ac939ce91afd0e383a348->enter($__internal_491a6307d48c8e7b0520ad96e7e22d0e41580bba4e2ac939ce91afd0e383a348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_491a6307d48c8e7b0520ad96e7e22d0e41580bba4e2ac939ce91afd0e383a348->leave($__internal_491a6307d48c8e7b0520ad96e7e22d0e41580bba4e2ac939ce91afd0e383a348_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_646ff34046177193749d8fe457396f8ad0905182bb72a73f014a3346a3a7dddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_646ff34046177193749d8fe457396f8ad0905182bb72a73f014a3346a3a7dddf->enter($__internal_646ff34046177193749d8fe457396f8ad0905182bb72a73f014a3346a3a7dddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_646ff34046177193749d8fe457396f8ad0905182bb72a73f014a3346a3a7dddf->leave($__internal_646ff34046177193749d8fe457396f8ad0905182bb72a73f014a3346a3a7dddf_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
