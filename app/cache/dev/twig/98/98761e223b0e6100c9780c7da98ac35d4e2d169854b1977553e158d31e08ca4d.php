<?php

/* :patient:new-json.html.twig */
class __TwigTemplate_9221b9507292fb0163f5337d3b3a2311facc947284b0f259285ed39c1d502342 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":patient:new-json.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70a175737104d8267d3ea1e4dc65ff2b35a1212bacd14b57a8c4c42bb544953d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70a175737104d8267d3ea1e4dc65ff2b35a1212bacd14b57a8c4c42bb544953d->enter($__internal_70a175737104d8267d3ea1e4dc65ff2b35a1212bacd14b57a8c4c42bb544953d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":patient:new-json.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_70a175737104d8267d3ea1e4dc65ff2b35a1212bacd14b57a8c4c42bb544953d->leave($__internal_70a175737104d8267d3ea1e4dc65ff2b35a1212bacd14b57a8c4c42bb544953d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e3babefa28894c05a32d70b739ef072b29c936ffff4feaaebc53f35ace16ec94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3babefa28894c05a32d70b739ef072b29c936ffff4feaaebc53f35ace16ec94->enter($__internal_e3babefa28894c05a32d70b739ef072b29c936ffff4feaaebc53f35ace16ec94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Patient creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("patient_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_e3babefa28894c05a32d70b739ef072b29c936ffff4feaaebc53f35ace16ec94->leave($__internal_e3babefa28894c05a32d70b739ef072b29c936ffff4feaaebc53f35ace16ec94_prof);

    }

    public function getTemplateName()
    {
        return ":patient:new-json.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Patient creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('patient_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":patient:new-json.html.twig", "/var/www/html/test/app/Resources/views/patient/new-json.html.twig");
    }
}
