<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_7ea2f7cf503859b1231ae225aca0055dcde690b6fd211cdbbb4f1d26e163f912 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1154889bd6b0cb810e062c34d4247f56b5fa8c6313c3e8709d6e6eb9268df4dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1154889bd6b0cb810e062c34d4247f56b5fa8c6313c3e8709d6e6eb9268df4dc->enter($__internal_1154889bd6b0cb810e062c34d4247f56b5fa8c6313c3e8709d6e6eb9268df4dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_1154889bd6b0cb810e062c34d4247f56b5fa8c6313c3e8709d6e6eb9268df4dc->leave($__internal_1154889bd6b0cb810e062c34d4247f56b5fa8c6313c3e8709d6e6eb9268df4dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
