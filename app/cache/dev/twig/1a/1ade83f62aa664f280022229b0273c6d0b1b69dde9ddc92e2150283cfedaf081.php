<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_54e9f5a87d1acb97c92180660dfcf9ce69591274568582e249ed2092685734c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ab6bbacc2f3feadd8b7ceab5c6ee59a4d58821d1dcb569dbd3ab97a13f419d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ab6bbacc2f3feadd8b7ceab5c6ee59a4d58821d1dcb569dbd3ab97a13f419d0->enter($__internal_3ab6bbacc2f3feadd8b7ceab5c6ee59a4d58821d1dcb569dbd3ab97a13f419d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_3ab6bbacc2f3feadd8b7ceab5c6ee59a4d58821d1dcb569dbd3ab97a13f419d0->leave($__internal_3ab6bbacc2f3feadd8b7ceab5c6ee59a4d58821d1dcb569dbd3ab97a13f419d0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
