<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_29ccb847ee67d18e428eafe8ce6881f3a5769d8ece4f250e646eeb379e63a970 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c50ab6e4b9b2cd7b4190b9ad236d723ef4976c495bf78695827b02af6da1404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c50ab6e4b9b2cd7b4190b9ad236d723ef4976c495bf78695827b02af6da1404->enter($__internal_2c50ab6e4b9b2cd7b4190b9ad236d723ef4976c495bf78695827b02af6da1404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_2c50ab6e4b9b2cd7b4190b9ad236d723ef4976c495bf78695827b02af6da1404->leave($__internal_2c50ab6e4b9b2cd7b4190b9ad236d723ef4976c495bf78695827b02af6da1404_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
