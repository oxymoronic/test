<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_0067a5744c24b6a52503299469f48b4768e631a6476d22c7bf88c05d38c56f48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62900128cd96c724444f42a1a5cde247258bcaa9d5f3a0a07e1ff3efa967cb16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62900128cd96c724444f42a1a5cde247258bcaa9d5f3a0a07e1ff3efa967cb16->enter($__internal_62900128cd96c724444f42a1a5cde247258bcaa9d5f3a0a07e1ff3efa967cb16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.rdf.twig", 1)->display($context);
        
        $__internal_62900128cd96c724444f42a1a5cde247258bcaa9d5f3a0a07e1ff3efa967cb16->leave($__internal_62900128cd96c724444f42a1a5cde247258bcaa9d5f3a0a07e1ff3efa967cb16_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.rdf.twig", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
