<?php

/* :doctor:new.html.twig */
class __TwigTemplate_f886cac3f4baeea6521efc1b4e4b7b7de771ee930bedec4b1a65ebafff92398f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":doctor:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_885c5a95ed1687252a448d711b781b357499e6687a4cb3aec9a69652593dde91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_885c5a95ed1687252a448d711b781b357499e6687a4cb3aec9a69652593dde91->enter($__internal_885c5a95ed1687252a448d711b781b357499e6687a4cb3aec9a69652593dde91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":doctor:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_885c5a95ed1687252a448d711b781b357499e6687a4cb3aec9a69652593dde91->leave($__internal_885c5a95ed1687252a448d711b781b357499e6687a4cb3aec9a69652593dde91_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_94289f6dd189cde2da79341e88e822ddd281689c89035de15062f611aca590a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94289f6dd189cde2da79341e88e822ddd281689c89035de15062f611aca590a3->enter($__internal_94289f6dd189cde2da79341e88e822ddd281689c89035de15062f611aca590a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Doctor creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_94289f6dd189cde2da79341e88e822ddd281689c89035de15062f611aca590a3->leave($__internal_94289f6dd189cde2da79341e88e822ddd281689c89035de15062f611aca590a3_prof);

    }

    public function getTemplateName()
    {
        return ":doctor:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Doctor creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('doctor_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":doctor:new.html.twig", "/var/www/html/test/app/Resources/views/doctor/new.html.twig");
    }
}
