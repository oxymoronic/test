<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_fec1dbc072251e8f0d649122065799f4cf32db69baa71cf0bca8436314fe31cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d88644e49d177efd7515b9695de5062a95af27b2571c4ce527bc4a2eb4d9881 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d88644e49d177efd7515b9695de5062a95af27b2571c4ce527bc4a2eb4d9881->enter($__internal_4d88644e49d177efd7515b9695de5062a95af27b2571c4ce527bc4a2eb4d9881_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_4d88644e49d177efd7515b9695de5062a95af27b2571c4ce527bc4a2eb4d9881->leave($__internal_4d88644e49d177efd7515b9695de5062a95af27b2571c4ce527bc4a2eb4d9881_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
