<?php

/* doctor/show.html.twig */
class __TwigTemplate_1693b9c56cf027bc6b13cdd385c1f0f4cdff1d7e841823b5450e71e3df295866 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "doctor/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef1f74412012635cb93e1b268e1de0ecbc84f7d05efe45945a25101c9745ff25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef1f74412012635cb93e1b268e1de0ecbc84f7d05efe45945a25101c9745ff25->enter($__internal_ef1f74412012635cb93e1b268e1de0ecbc84f7d05efe45945a25101c9745ff25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "doctor/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ef1f74412012635cb93e1b268e1de0ecbc84f7d05efe45945a25101c9745ff25->leave($__internal_ef1f74412012635cb93e1b268e1de0ecbc84f7d05efe45945a25101c9745ff25_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_407e5adf5425d803c31ad39ccabf91ba8ccd71450a99757368b27ce8be15fb45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_407e5adf5425d803c31ad39ccabf91ba8ccd71450a99757368b27ce8be15fb45->enter($__internal_407e5adf5425d803c31ad39ccabf91ba8ccd71450a99757368b27ce8be15fb45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Doctor</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "name", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_edit", array("id" => $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_407e5adf5425d803c31ad39ccabf91ba8ccd71450a99757368b27ce8be15fb45->leave($__internal_407e5adf5425d803c31ad39ccabf91ba8ccd71450a99757368b27ce8be15fb45_prof);

    }

    public function getTemplateName()
    {
        return "doctor/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  77 => 27,  71 => 24,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Doctor</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ doctor.id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ doctor.name }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('doctor_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('doctor_edit', { 'id': doctor.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "doctor/show.html.twig", "/var/www/html/test/app/Resources/views/doctor/show.html.twig");
    }
}
