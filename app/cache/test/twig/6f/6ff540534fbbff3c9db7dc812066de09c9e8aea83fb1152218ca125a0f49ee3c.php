<?php

/* patient/index.html.twig */
class __TwigTemplate_e6cd93f1a1abe93812882cf9ebe1bf97f35cb38fc5f499ad48ed7a139bd54288 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "patient/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_689d6d4e932fa5816db6d372ff528500037edc6160337c762897aa927c33ec31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_689d6d4e932fa5816db6d372ff528500037edc6160337c762897aa927c33ec31->enter($__internal_689d6d4e932fa5816db6d372ff528500037edc6160337c762897aa927c33ec31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "patient/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_689d6d4e932fa5816db6d372ff528500037edc6160337c762897aa927c33ec31->leave($__internal_689d6d4e932fa5816db6d372ff528500037edc6160337c762897aa927c33ec31_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_50656792f44162d6eed1a38d3997b6db3f0688d44dfbae8b7b8152706e267147 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50656792f44162d6eed1a38d3997b6db3f0688d44dfbae8b7b8152706e267147->enter($__internal_50656792f44162d6eed1a38d3997b6db3f0688d44dfbae8b7b8152706e267147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Patients list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Dob</th>
                <th>Gender</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["patients"]) ? $context["patients"] : $this->getContext($context, "patients")));
        foreach ($context['_seq'] as $context["_key"] => $context["patient"]) {
            // line 17
            echo "            <tr>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["patient"], "id", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["patient"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            if ($this->getAttribute($context["patient"], "dob", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["patient"], "dob", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["genders"]) ? $context["genders"] : $this->getContext($context, "genders")), $this->getAttribute($context["patient"], "gender", array()), array(), "array"), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['patient'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("patient_new_json");
        echo "\">Create a new patient</a>
        </li>
    </ul>
";
        
        $__internal_50656792f44162d6eed1a38d3997b6db3f0688d44dfbae8b7b8152706e267147->leave($__internal_50656792f44162d6eed1a38d3997b6db3f0688d44dfbae8b7b8152706e267147_prof);

    }

    public function getTemplateName()
    {
        return "patient/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  84 => 24,  75 => 21,  69 => 20,  65 => 19,  61 => 18,  58 => 17,  54 => 16,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Patients list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Dob</th>
                <th>Gender</th>
            </tr>
        </thead>
        <tbody>
        {% for patient in patients %}
            <tr>
                <td>{{ patient.id }}</td>
                <td>{{ patient.name }}</td>
                <td>{% if patient.dob %}{{ patient.dob|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{{ genders[patient.gender] }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('patient_new_json') }}\">Create a new patient</a>
        </li>
    </ul>
{% endblock %}
", "patient/index.html.twig", "/var/www/html/test/app/Resources/views/patient/index.html.twig");
    }
}
