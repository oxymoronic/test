<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_590bedb686c0934df5d6704e564c981e4a841b9e71c275323f574a707c2ffae4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d27b555e32547b6390974308c41328730509326e0b7db4c2cdbfaa5a8ee318c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d27b555e32547b6390974308c41328730509326e0b7db4c2cdbfaa5a8ee318c0->enter($__internal_d27b555e32547b6390974308c41328730509326e0b7db4c2cdbfaa5a8ee318c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d27b555e32547b6390974308c41328730509326e0b7db4c2cdbfaa5a8ee318c0->leave($__internal_d27b555e32547b6390974308c41328730509326e0b7db4c2cdbfaa5a8ee318c0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0a33e6b79d4065f5b3b085b5ab01ea1dd203f93cd978daf25efd68e43884699f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a33e6b79d4065f5b3b085b5ab01ea1dd203f93cd978daf25efd68e43884699f->enter($__internal_0a33e6b79d4065f5b3b085b5ab01ea1dd203f93cd978daf25efd68e43884699f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_0a33e6b79d4065f5b3b085b5ab01ea1dd203f93cd978daf25efd68e43884699f->leave($__internal_0a33e6b79d4065f5b3b085b5ab01ea1dd203f93cd978daf25efd68e43884699f_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_7561b47f6f09c12cb2bea007d15575e82d63e382c3f010f7adf56b04bdb64835 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7561b47f6f09c12cb2bea007d15575e82d63e382c3f010f7adf56b04bdb64835->enter($__internal_7561b47f6f09c12cb2bea007d15575e82d63e382c3f010f7adf56b04bdb64835_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_7561b47f6f09c12cb2bea007d15575e82d63e382c3f010f7adf56b04bdb64835->leave($__internal_7561b47f6f09c12cb2bea007d15575e82d63e382c3f010f7adf56b04bdb64835_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_3bb4cabe275cde2de26ada2a2de42d47d619ce0e17a211531d567de227e564ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bb4cabe275cde2de26ada2a2de42d47d619ce0e17a211531d567de227e564ae->enter($__internal_3bb4cabe275cde2de26ada2a2de42d47d619ce0e17a211531d567de227e564ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_3bb4cabe275cde2de26ada2a2de42d47d619ce0e17a211531d567de227e564ae->leave($__internal_3bb4cabe275cde2de26ada2a2de42d47d619ce0e17a211531d567de227e564ae_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/html/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
