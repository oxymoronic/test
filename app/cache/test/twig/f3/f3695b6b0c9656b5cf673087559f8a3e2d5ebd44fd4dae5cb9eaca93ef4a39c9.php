<?php

/* base.html.twig */
class __TwigTemplate_8c574b2cd07997c67feb95440e9fbcac5a26e98453a48814b91963c6fbd261d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6c4c7f722721fe9c67a0a1fe161436864c5b0d274048bbb7446f5cdd9174983 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6c4c7f722721fe9c67a0a1fe161436864c5b0d274048bbb7446f5cdd9174983->enter($__internal_f6c4c7f722721fe9c67a0a1fe161436864c5b0d274048bbb7446f5cdd9174983_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_f6c4c7f722721fe9c67a0a1fe161436864c5b0d274048bbb7446f5cdd9174983->leave($__internal_f6c4c7f722721fe9c67a0a1fe161436864c5b0d274048bbb7446f5cdd9174983_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_85222e4a89962a734e25a922910bbbc4b17060dd96a20814f1e0b301c0cb22f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85222e4a89962a734e25a922910bbbc4b17060dd96a20814f1e0b301c0cb22f6->enter($__internal_85222e4a89962a734e25a922910bbbc4b17060dd96a20814f1e0b301c0cb22f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_85222e4a89962a734e25a922910bbbc4b17060dd96a20814f1e0b301c0cb22f6->leave($__internal_85222e4a89962a734e25a922910bbbc4b17060dd96a20814f1e0b301c0cb22f6_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7cfacdf2b92a234f9a9d276fb2d41df1506d2da7547d9b74da9c2099069d30d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7cfacdf2b92a234f9a9d276fb2d41df1506d2da7547d9b74da9c2099069d30d6->enter($__internal_7cfacdf2b92a234f9a9d276fb2d41df1506d2da7547d9b74da9c2099069d30d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_7cfacdf2b92a234f9a9d276fb2d41df1506d2da7547d9b74da9c2099069d30d6->leave($__internal_7cfacdf2b92a234f9a9d276fb2d41df1506d2da7547d9b74da9c2099069d30d6_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_0c29bd6697b2f0dbb496b8a793f1842d086bef46905876c2719bad6581390576 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c29bd6697b2f0dbb496b8a793f1842d086bef46905876c2719bad6581390576->enter($__internal_0c29bd6697b2f0dbb496b8a793f1842d086bef46905876c2719bad6581390576_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_0c29bd6697b2f0dbb496b8a793f1842d086bef46905876c2719bad6581390576->leave($__internal_0c29bd6697b2f0dbb496b8a793f1842d086bef46905876c2719bad6581390576_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_bd889269cd6094f742f3c295d2ed7e331cd42c08f567ae047d213d32a6c2fd70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd889269cd6094f742f3c295d2ed7e331cd42c08f567ae047d213d32a6c2fd70->enter($__internal_bd889269cd6094f742f3c295d2ed7e331cd42c08f567ae047d213d32a6c2fd70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_bd889269cd6094f742f3c295d2ed7e331cd42c08f567ae047d213d32a6c2fd70->leave($__internal_bd889269cd6094f742f3c295d2ed7e331cd42c08f567ae047d213d32a6c2fd70_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/test/app/Resources/views/base.html.twig");
    }
}
