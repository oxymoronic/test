<?php

/* doctor/new.html.twig */
class __TwigTemplate_f886cac3f4baeea6521efc1b4e4b7b7de771ee930bedec4b1a65ebafff92398f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "doctor/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a48a6dca67e074570fba41cb2c3f9d98f371d7e5860502e5a127f695972683f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a48a6dca67e074570fba41cb2c3f9d98f371d7e5860502e5a127f695972683f->enter($__internal_6a48a6dca67e074570fba41cb2c3f9d98f371d7e5860502e5a127f695972683f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "doctor/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6a48a6dca67e074570fba41cb2c3f9d98f371d7e5860502e5a127f695972683f->leave($__internal_6a48a6dca67e074570fba41cb2c3f9d98f371d7e5860502e5a127f695972683f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9f2f9e80797e6d7b57f029e5db95f6ce161cd0d97e0318c25cc0b6b998cde8ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f2f9e80797e6d7b57f029e5db95f6ce161cd0d97e0318c25cc0b6b998cde8ac->enter($__internal_9f2f9e80797e6d7b57f029e5db95f6ce161cd0d97e0318c25cc0b6b998cde8ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Doctor creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctor_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_9f2f9e80797e6d7b57f029e5db95f6ce161cd0d97e0318c25cc0b6b998cde8ac->leave($__internal_9f2f9e80797e6d7b57f029e5db95f6ce161cd0d97e0318c25cc0b6b998cde8ac_prof);

    }

    public function getTemplateName()
    {
        return "doctor/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Doctor creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('doctor_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "doctor/new.html.twig", "/var/www/html/test/app/Resources/views/doctor/new.html.twig");
    }
}
