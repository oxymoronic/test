<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/doctor')) {
            // doctor_index
            if (rtrim($pathinfo, '/') === '/doctor') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'doctor_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\DoctorController::indexAction',  '_route' => 'doctor_index',);
            }
            not_doctor_index:

            // doctor_new
            if ($pathinfo === '/doctor/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_doctor_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\DoctorController::newAction',  '_route' => 'doctor_new',);
            }
            not_doctor_new:

            // doctor_show
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_show')), array (  '_controller' => 'AppBundle\\Controller\\DoctorController::showAction',));
            }
            not_doctor_show:

            // doctor_edit
            if (preg_match('#^/doctor/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_doctor_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_edit')), array (  '_controller' => 'AppBundle\\Controller\\DoctorController::editAction',));
            }
            not_doctor_edit:

            // doctor_delete
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_doctor_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_delete')), array (  '_controller' => 'AppBundle\\Controller\\DoctorController::deleteAction',));
            }
            not_doctor_delete:

        }

        if (0 === strpos($pathinfo, '/hospital')) {
            // hospital_index
            if (rtrim($pathinfo, '/') === '/hospital') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_hospital_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'hospital_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\HospitalController::indexAction',  '_route' => 'hospital_index',);
            }
            not_hospital_index:

            // hospital_new
            if ($pathinfo === '/hospital/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_hospital_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\HospitalController::newAction',  '_route' => 'hospital_new',);
            }
            not_hospital_new:

            // hospital_show
            if (preg_match('#^/hospital/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_hospital_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hospital_show')), array (  '_controller' => 'AppBundle\\Controller\\HospitalController::showAction',));
            }
            not_hospital_show:

            // hospital_edit
            if (preg_match('#^/hospital/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_hospital_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hospital_edit')), array (  '_controller' => 'AppBundle\\Controller\\HospitalController::editAction',));
            }
            not_hospital_edit:

            // hospital_delete
            if (preg_match('#^/hospital/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_hospital_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hospital_delete')), array (  '_controller' => 'AppBundle\\Controller\\HospitalController::deleteAction',));
            }
            not_hospital_delete:

        }

        if (0 === strpos($pathinfo, '/patient')) {
            // patient_index
            if (rtrim($pathinfo, '/') === '/patient') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_patient_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'patient_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\PatientController::indexAction',  '_route' => 'patient_index',);
            }
            not_patient_index:

            if (0 === strpos($pathinfo, '/patient/new-json')) {
                // patient_new_json
                if ($pathinfo === '/patient/new-json') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_patient_new_json;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\PatientController::newJsonAction',  '_route' => 'patient_new_json',);
                }
                not_patient_new_json:

                // patient_new_json_save
                if ($pathinfo === '/patient/new-json-save') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_patient_new_json_save;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\PatientController::newJsonSaveAction',  '_route' => 'patient_new_json_save',);
                }
                not_patient_new_json_save:

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
