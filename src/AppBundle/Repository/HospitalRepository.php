<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

use AppBundle\Entity\Hospital;

class HospitalRepository extends EntityRepository implements RepositoryInterface
{
	/** @return Hospital */
	public function selectById($id)
	{

		return $this->getEntityManager()
                            ->find('AppBundle\Entity\Hospital', $id);
	}
}
