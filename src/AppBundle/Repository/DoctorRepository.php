<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Doctor;
use AppBundle\Entity\Patient;

class DoctorRepository extends EntityRepository implements RepositoryInterface
{
	/** @return Patient */
	public function selectById($id)
	{
	
		return $this->getEntityManager()
                            ->find('AppBundle\Entity\Doctor', $id);
	}

	/**
	 * @param \AppBundle\Entity\Patient $patient
	 * @return Doctor[]
	 */
	public function selectByPatient(Patient $patient){}


	public function findAll() {

    		$q = $this->getEntityManager()
			  ->createQuery("SELECT d FROM AppBundle\Entity\Doctor d");
		
		return $q->getResult();
	}	
}
