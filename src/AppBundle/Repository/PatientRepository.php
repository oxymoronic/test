<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Patient;
use AppBundle\Entity\Hospital;

class PatientRepository extends EntityRepository implements RepositoryInterface
{
	/** @return Patient */
	public function selectById($id)
	{
	
		return $this->getEntityManager()
                            ->find('AppBundle\Entity\Patient', $id);

	}

	/**
	 * @param \AppBundle\Entity\Hospital $hospital
	 * @return Patient[]
	 */
	public function selectByHospital(Hospital $hospital){
		
		$q = $this->getEntityManager()
                ->createQuery("SELECT p FROM AppBundle\Entity\Patient p
				JOIN p.hospital h 
				WHERE h.id = " . $hospital->getId() );
                
		return $q->getResult();
	}

	/**
         * @param \AppBundle\Entity\Doctor $doctor
         * @return Patient[]
         */
        public function selectByDoctor(Doctor $doctor){

                $q = $this->getEntityManager()
                ->createQuery("SELECT p FROM AppBundle\Entity\Patient p
                                JOIN p.doctor d
                                WHERE d.id = " . $doctor->getId() );

                return $q->getResult();
        }

	public function findAll() {

    		$q = $this->getEntityManager()
		->createQuery("SELECT p FROM AppBundle\Entity\Patient p");
		
		return $q->getResult();
	}	
}
