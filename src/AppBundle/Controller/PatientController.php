<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Patient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Patient controller.
 *
 * @Route("patient")
 */
class PatientController extends Controller
{
    /**
     * Lists all patient entities.
     *
     * @Route("/", name="patient_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $patients = $em->getRepository('AppBundle:Patient')->findAll();

        return $this->render('patient/index.html.twig', array(
            'patients' => $patients,
	    'genders'  => [Patient::GENDER_MALE   => 'Male',
        		  Patient::GENDER_FEMALE => 'Female',
        		  Patient::GENDER_OTHER  => 'Other']
        ));
    }
    
    /**
     * Hosts a form for creating a new patient.
     *
     * @Route("/new-json", name="patient_new_json")
     * @Method("GET")
     */
    public function newJsonAction(Request $request)
    {
        $patient = new Patient();
	
        $form = $this->createForm('AppBundle\Form\PatientJsonType', $patient, ['action' => $this->generateUrl('patient_new_json_save')]);
        $form->handleRequest($request);
        
	return $this->render('patient/new-json.html.twig', array(
            'patient' => $patient,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * Creates a new patient entity.
     * Response in JSON
     *
     * @Route("/new-json-save", name="patient_new_json_save")
     * @Method({"POST"})
     */
    public function newJsonSaveAction(Request $request)
    {
	$responseCode = 200;
	$doctor   = null;
	$patients = null;
	$msg      = null;
        $patient  = new Patient();
        $form     = $this->createForm('AppBundle\Form\PatientJsonType', $patient);
	$em       = $this->getDoctrine()->getManager();

	try {
	    $form->handleRequest($request);
	    if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($patient);
                $em->flush($patient);
	        
		$doctor   = $patient->getDoctor();
	        $patients = $em->getRepository('AppBundle:Patient')->findByDoctor($doctor);
		$responseCode = 201;//successfully created
            }
	    else {
	        $msg = $this->getErrorMessages($form);
		$responseCode = 400;
	    }
	}
	catch(Exception $e) {
	   $msg = $e->getMessage();
	   $responseCode = 500;
	}

	// Return a list of patients along with the original hospital and a message showing success
	return new JsonResponse([
		'doctor'   => $doctor,
		'patients' => $patients,
		'msg'      => $msg
	], $responseCode);
    }


    /**
     * Get error messages so that we can format them in json response
     *
     * @param (\Symfony\Component\Form\Form $form Contais evaluation logic an error messages
     * @return array
     */
    private function getErrorMessages(\Symfony\Component\Form\Form $form) 
    {
        $errors = array();

	$fields = $form->all();
	foreach ( $fields as $field ) {
   	    if(!$field->isValid()) {
	        $errors[] = $field->getErrorsAsString();
            }
        }

        return $errors;
    }
}
