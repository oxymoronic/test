<?php

namespace AppBundle\Form;

use AppBundle\Entity\Patient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('dob', 'date', ['years' => range(1900, date('Y'))])
                ->add('gender', 'choice', ['choices' => [Patient::GENDER_MALE => 'Male',
                                                         Patient::GENDER_FEMALE => 'Female',
                                                         Patient::GENDER_OTHER => 'Other']])
                ->add('hospital')
		->add('doctor');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Patient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_patient';
    }


}
