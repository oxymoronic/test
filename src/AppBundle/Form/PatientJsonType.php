<?php

namespace AppBundle\Form;

use AppBundle\Entity\Patient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class PatientJsonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',     'text',   ['required' => true,
					     'constraints' => [new NotBlank(['message' => 'name field should not be blank']) ]])
                ->add('dob',      'date',   ['years' => range(1900, date('Y')), 
				             'required' => true,
					     'invalid_message' => 'dob (date of birth) field contains a date and should be provided as dob[year]=yyyy&dob[month]=mm&dob[day]=dd',
					     'constraints' => [new NotBlank(['message' => 'dob (date of birth) contains a date and should be provided as dob[year]=yyyy&dob[month]=mm&dob[day]=dd']),
							       new Date(['message' => 'dob Must be provided in a valid format']) ]])
                ->add('gender',   'choice', ['choices' => [Patient::GENDER_MALE => 'Male',
                                                           Patient::GENDER_FEMALE => 'Female',
                                                           Patient::GENDER_OTHER => 'Other'],
					     'required' => true,
					     'constraints' => [new NotBlank(['message' => 'gender field should not be blank']) ]])
                ->add('hospital', 'entity', ['class' => 'AppBundle:Hospital', 
                                            'required' => true,
					     'constraints' => [new NotBlank(['message' => 'hospital field should not be blank']) ]])
		->add('doctor',   'entity', ['class' => 'AppBundle:Doctor',
					     'required' => true,
					     'constraints' => [new NotBlank(['message' => 'doctor field should not be blank']) ]]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Patient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_patient';
    }


}
