<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Hospital;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Patient
 *
 * @ORM\Table(name="patient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PatientRepository")
 */
class Patient implements JsonSerializable {
	
	const GENDER_MALE   = 1;
	const GENDER_FEMALE = 2;
	const GENDER_OTHER  = 3;

	/**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
	private $id;
	
        /**
         * @var string
         *
         * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
     	 * Date of Birth
	 * @var \DateTime
    	 *
    	 * @ORM\Column(name="dob", type="datetime")
	 */
	private $dob;
	
	/**
         * @var string
         *
         * @ORM\Column(name="gender", type="integer")
	 */
	private $gender;
	
	/** 
	 * @var Hospital 
    	 * Many Patients have One Hospital. A Hospital has many Patients
     	 * @ORM\ManyToOne(targetEntity="Hospital")
    	 * @ORM\JoinColumn(name="hospital_id", referencedColumnName="id")
         */
	private $hospital;

	/**
         * @var Doctor
         * Many Patients have One Doctor. A Doctor has many Patients
         * @ORM\ManyToOne(targetEntity="Doctor")
         * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
	 */
        private $doctor;
	
	/**
         * Use name when casting to string
         */
        public function __toString()
        {

                return $this->getName();
        }
	
	/**
         * What info to display when entity is being cast to string for json
         */
	public function jsonSerialize()
	    {
	        return ['id'    => $this->getId(),
			'name'  => $this->getName(),
			'dob'   => $this->getDob(),
			'gender' => $this->getStringGender()];
	}

	
	/**
	 * @return int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Patient
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{

		return $this->name;
	}

	/**
	 * @param string $name
	 * @return Patient
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDob()
	{

		return $this->dob;
	}

	/**
	 * @param \DateTime $dob
	 * @return Patient
	 */
	public function setDob($dob)
	{
		$this->dob = $dob;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getGender()
	{

		return $this->gender;
	}

	/**
         * Get string representation of gender
	 * @return string
         */
        public function getStringGender()
        {
		$lookup = [self::GENDER_MALE   => 'Male', 
			   self::GENDER_FEMALE => 'Female', 
			   self::GENDER_OTHER  => 'Other'];

                return $lookup[$this->gender];
        }

	/**
	 * @param string $gender
	 * @return Patient
	 */
	public function setGender($gender)
	{
		if (!in_array($gender, [self::GENDER_MALE, self::GENDER_FEMALE, self::GENDER_OTHER])) {
            		throw new \InvalidArgumentException("Invalid gender");
        	}
		$this->gender = $gender;

		return $this;
	}

	/**
	 * @return Hospital
	 */
	public function getHospital()
	{

		return $this->hospital;
	}

	/**
	 * @param Hospital $hospital
	 * @return Patient
	 */
	public function setHospital($hospital)
	{
		$this->hospital = $hospital;

		return $this;
	}

	/**
         * @return Doctor
         */
        public function getDoctor()
        {

                return $this->doctor;
        }

        /**
         * @param Doctor $doctor
         * @return Patient
         */
        public function setDoctor($doctor)
        {
                $this->doctor = $doctor;

                return $this;
        }

}
