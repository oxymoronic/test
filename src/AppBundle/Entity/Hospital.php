<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hospital
 *
 * @ORM\Table(name="hospital")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HospitalRepository")
 */
class Hospital
{
	/**
    	 * @var int
    	 *
    	 * @ORM\Column(name="id", type="integer")
    	 * @ORM\Id
     	 * @ORM\GeneratedValue(strategy="AUTO")
     	 */
	private $id;
	
	/**
    	 * @var string
     	 *
    	 * @ORM\Column(name="name", type="string", length=255)
     	 */
	private $name;

        /**
         * Use name when casting to string
         */
        public function __toString()
        {

                return $this->getName();
        }
	
	/**
         * What info to display when entity is being cast to string for json
         */
        public function jsonSerialize()
        {
            return [
                'id' => $this->getId(),
                'name'=> $this->getName(),
            ];
        }

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Hospital
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 * @return Hospital
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

}
