<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Doctor
 *
 * @ORM\Table(name="doctor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DoctorRepository")
 */
class Doctor implements JsonSerializable{
	/**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
	private $id;
	
        /**
         * @var string
         *
         * @ORM\Column(name="name", type="string", length=255)
         */
	private $name;

	/**
         * Use name when casting to string
         */
        public function __toString()
        {

                return $this->getName();
        }
	
	/**
         * What info to display when entity is being cast to string for json
         */
	public function jsonSerialize()
	{
            return [
            	'id' => $this->getId(),
            	'name'=> $this->getName(),
       	    ];
	}

	
	/**
	 * @return int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Patient
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{

		return $this->name;
	}

	/**
	 * @param string $name
	 * @return Patient
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

}
